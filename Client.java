package barbershop;

import java.util.Random;
import java.util.List;

public class Client {
    private int waitedTime = 0;
    private final boolean wantsBeardTrimming;
    private static int idGenerator = 0;
    private int id = idGenerator++;

    public int getId() {
        return id;
    }

    public Client() {
        Random random = new Random();
        this.wantsBeardTrimming = random.nextBoolean();
    }

    public boolean wantsBeardTrimming() {
        return this.wantsBeardTrimming;
    }

    public void waiting() {
        this.waitedTime += 1;
    }

    public int getWaitedTime() {
        return this.waitedTime;
    }

    public void done() {
        synchronized(this) {
            this.notify();
        }
    }

    public void greetingToBarbers(List < Barber > barbers) {
        if (this.wantsBeardTrimming()) {
            barbers.get(1).greeting();
        } else {
            for (Barber barber: barbers) {
                barber.greeting();
            }
        }
    }
}