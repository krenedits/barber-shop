package barbershop;

public class Barber {
    private boolean isBeardTrimmer;
    private int servedClients = 0;

    public Barber(boolean isBeardTrimmer) {
        this.isBeardTrimmer = isBeardTrimmer;
    }

    public boolean getIsBeardTrimmer() {
        return this.isBeardTrimmer;
    }

    public void greeting() {
        synchronized(this) {
            this.notify();
        }
    }

    public int getServedClients() {
        return this.servedClients;
    }

    public void servedClient() {
        this.servedClients++;
    }

    public void waiting() {
        try {
            synchronized(this) {
                this.wait();
            }
        } catch (InterruptedException e) {
            this.notify();
        }
    }
}