package barbershop;

import java.util.concurrent.TimeUnit;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ThreadLocalRandom;
import java.util.Random;
import java.util.Optional;

public class BarberShopSimulator {

    // ---- KONSTANSOK ----
    private static final int MAX_NUMBER_OF_CLIENTS = 5;
    private static final int MAX_NUMBER_OF_THREADS = 100;
    private static final int SERVICE_TIME_MIN = 20;
    private static final int SERVICE_TIME_MAX = 200;
    private static final int CLIENT_FREQUENCY = 40; //óránként 10 ügyfél

    // ---- VALTOZOK ----
    private static final List < Client > clients = Collections.synchronizedList(new ArrayList < > ());
    private static final List < Barber > barbers = Collections.synchronizedList(new ArrayList < > ());
    private static final AtomicInteger servedClients = new AtomicInteger(0);
    private static final AtomicInteger waitedTimeAll = new AtomicInteger(0);
    private static final AtomicInteger nonServedClientsBecauseOfFullHouse = new AtomicInteger(0);
    private static final AtomicInteger nonServedClientsBecauseOfClosedShop = new AtomicInteger(0);
    private static final AtomicInteger servedClientsToday = new AtomicInteger(0);
    private static final List < Integer > servedClientsByDay = Collections.synchronizedList(new ArrayList < > ());
    private static final Time time = new Time();

    // ---- EXECUTOROK ----
    private static final ScheduledExecutorService timeExecutor = Executors.newScheduledThreadPool(1);
    private static final ScheduledExecutorService clientExecutor = Executors.newScheduledThreadPool(1);
    private static final ExecutorService cleanupExecutor = Executors.newFixedThreadPool(MAX_NUMBER_OF_THREADS);
    private static final ExecutorService barberExecutor = Executors.newFixedThreadPool(2);
    private static final ScheduledThreadPoolExecutor clientWaitingExecutor = (ScheduledThreadPoolExecutor) Executors.newScheduledThreadPool(5);


    public static void main(String[] args) {
		clientWaitingExecutor.setRemoveOnCancelPolicy(true);
		
        timeExecutor.scheduleAtFixedRate(() -> timeConsuming(), 1, 1, TimeUnit.MILLISECONDS);
        clientExecutor.scheduleAtFixedRate(() -> createClient(), CLIENT_FREQUENCY, CLIENT_FREQUENCY, TimeUnit.MILLISECONDS);
        barberExecutor.submit(() -> createBarber(false));
        barberExecutor.submit(() -> createBarber(true));
    }

    private static void createClient() {
        if (time.isInWorkingHours()) {
            clientVisitingShop();
        } else {
            nonServedClientsBecauseOfClosedShop.incrementAndGet();
        }
    }

    private static void clientVisitingShop() {
        if (clients.size() < MAX_NUMBER_OF_CLIENTS) {
            Client client = new Client();
            client.greetingToBarbers(barbers);
            clients.add(client);
            System.out.println("Vevo erkezett (azonosito: " + client.getId() + ")");
            cleanupExecutor.submit(() -> cleanupAction(client));
        } else {
            nonServedClientsBecauseOfFullHouse.incrementAndGet();
        }
    }

    private static void timeConsuming() {
        if (time.isEnd()) {
            end();
        } else if (time.isNewDay()) {
            newDay();
        } else if (time.isOpening()) {
            logOpening();
        } else if (time.isClosing()) {
            logClosing();
        }
        time.consuming();
    }

    private static void end() {
        servedClientsByDay.add(servedClientsToday.get());
        endSimulation();
    }

    private static void newDay() {
        logNewDay();
        if (time.getValue() > 0) {
            servedClientsByDay.add(servedClientsToday.get());
            servedClientsToday.set(0);
        }
    }

    private static void createBarber(boolean isBeardTrimmer) {
        Barber barber = new Barber(isBeardTrimmer);
        barbers.add(barber);
        lookingForClient(barber);
    }

    private static void lookingForClient(Barber barber) {
        if (clients.size() > 0) {
            manageClient(barber);
        } else {
            barber.waiting();
            lookingForClient(barber);
        }
    }

    private static void manageClient(Barber barber) {
        if (!barber.getIsBeardTrimmer()) {
            Optional < Client > clientInChair = clients.stream().filter(client -> !client.wantsBeardTrimming()).findFirst();
            clientInChair.ifPresentOrElse(
                client -> serveClient(client, barber, true),
                () -> {
                    barber.waiting();
                    lookingForClient(barber);
                }
            );
        } else {
            Client clientInChair = clients.get(0);
			if (clientInChair.wantsBeardTrimming()) {
				serveClient(clientInChair, barber, false); //szakállnyírás
			}
            serveClient(clientInChair, barber, true); //hajvágás
        }
    }

    private static void serveClient(Client client, Barber barber, boolean isHairCutting) {
        Random random = new Random();
        final int serviceTime = random.nextInt(SERVICE_TIME_MAX - SERVICE_TIME_MIN) + SERVICE_TIME_MIN;
        sleepForMsec(serviceTime);
        client.done();
        if (isHairCutting) {
            barber.servedClient();
            servedClients.incrementAndGet();
            servedClientsToday.incrementAndGet();
			lookingForClient(barber);
        }
    }

    private static void logNewDay() {
        System.out.println(time.getDay() + ". nap kezdete");
    }

    private static void logOpening() {
        System.out.println("Kinyitott az uzlet.");
    }

    private static void logClosing() {
        System.out.println("Bezart az uzlet.");
    }

    private static void cleanupAction(Client client) {
        try {
            synchronized(client) {
                ScheduledFuture<?> scheduledFuture = clientWaitingExecutor.scheduleAtFixedRate(() -> client.waiting(), 1, 1, TimeUnit.MILLISECONDS);
                client.wait();
                final String hadBeardTrimming = client.wantsBeardTrimming() ? "kert szakallnyirast" : "nem kert szakallnyirast";
                System.out.println("Vevo tavozott (azonosito: " + client.getId() + "; " + hadBeardTrimming + ")");
                waitedTimeAll.addAndGet(client.getWaitedTime());
                clients.remove(client);
				scheduledFuture.cancel(false);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void endSimulation() {
        logStatistics();
        timeExecutor.shutdownNow();
        clientExecutor.shutdownNow();
        cleanupExecutor.shutdownNow();
        clientWaitingExecutor.shutdownNow();
        barberExecutor.shutdownNow();
    }

    private static void printArray(List < Integer > array) {
        for (int i = 0; i < array.size(); i++) {
            System.out.println((i + 1) + ". nap kiszolgalva: " + array.get(i) + " ember");
        }
    }

    private static void logStatistics() {
        final int waitingTimeAverage = waitedTimeAll.get() / servedClients.get();
        System.out.println("\n--------SZIMULACIO VEGE--------");
        System.out.println(servedClients.get() + " ember lett kiszolgalva");
        System.out.println(nonServedClientsBecauseOfClosedShop.get() + " ember nem lett kiszolgalva, mert zarva volt az uzlet.");
        System.out.println(nonServedClientsBecauseOfFullHouse.get() + " ember nem lett kiszolgalva, mert telthaz volt.");
        System.out.println("A csak hajnyiros fodrasz kiszolgalt: " + barbers.get(0).getServedClients());
        System.out.println("A szakallnyiros fodrasz kiszolgalt: " + barbers.get(1).getServedClients());
        printArray(servedClientsByDay);
        System.out.println("Atlagos varakozasi ido: " + time.getHourByValue(waitingTimeAverage) + " ora (" + waitingTimeAverage + " valosideju milliszekundum)");
        System.out.println("--------------------------------");
    }

    private static void sleepForMsec(int msec) {
        try {
            TimeUnit.MILLISECONDS.sleep(msec);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}