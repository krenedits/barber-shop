package barbershop;

public class Time {
    private int value;
    private final int ONE_HOUR = 400;
    private final int ONE_DAY = 24 * ONE_HOUR;

    public Time() {
        this.value = 0; // in hour
    }

    public void consuming() {
        this.value++;
    }

    public int getValue() {
        return this.value;
    }

    public boolean isInWorkingHours() {
        return getHour() >= 9 && getHour() < 17;
    }

    public boolean isOpening() {
        return this.value % ONE_DAY == 9 * ONE_HOUR;
    }

    public boolean isClosing() {
        return this.value % ONE_DAY == 17 * ONE_HOUR;
    }

    public int getHour() {
        final int hour = (this.value / ONE_HOUR % 24);
        return hour;
    }

    public int getHourByValue(int value) {
        final int hour = (value / ONE_HOUR % 24);
        return hour;
    }

    public int getDay() {
        final int day = value / ONE_DAY + 1;
        return day;
    }

    public boolean isNewDay() {
        return this.value % ONE_DAY == 0;
    }

    public boolean isEnd() {
        return this.value >= ONE_DAY * 5 - 1;
    }
}